package pl.sda.shape;

import java.util.Scanner;

public class Rectangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadz wysokosc");
        final Integer n = Integer.valueOf(scanner.nextLine());
        System.out.println("Wprowadz szerokosc");
        final Integer m = Integer.valueOf(scanner.nextLine());

        //wypelniony
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println();
        //niewypelniony
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (j == m - 1 || i == n - 1 || i == 0 || j == 0) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
