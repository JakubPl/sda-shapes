package pl.sda.shape;

import java.util.Scanner;

public class EquilateralTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadz wysokosc");
        final Integer n = Integer.valueOf(scanner.nextLine());

        final int half = n / 2;
        //wypelniony
        for (int y = 0; y < n; y++) {
            for (int x = 0; x <= n*2 - 2; x++) {
                if(x >= n - y - 1 && x <= n + y - 1|| y == n -1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        System.out.println();

        //niewypelniony
        for (int y = 0; y < n; y++) {
            for (int x = 0; x <= n*2 - 2; x++) {
                if(x == n - y - 1 || x == n + y - 1|| y == n -1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        System.out.println();

        //wypelniony szachownica
        for (int y = 0; y < n; y++) {
            for (int x = 0; x <= n*2 - 2; x++) {
                if((x == n - y - 1 || x == n + y - 1|| y == n - 1) //brzegi
                        || x > n - y - 1 && x < n + y - 1 && ((x + y + 1) % 2) == 0) { // wnetrze
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        System.out.println();

    }
}
